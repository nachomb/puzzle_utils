import 'dart:async';
import 'dart:io';

import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:path/path.dart' show join;
import 'package:path_provider/path_provider.dart';
import 'package:image/image.dart' as img;
import 'package:shared_preferences/shared_preferences.dart';

Future<void> main() async {
  // Ensure that plugin services are initialized so that `availableCameras()`
  // can be called before `runApp()`
  WidgetsFlutterBinding.ensureInitialized();

  // Obtain a list of the available cameras on the device.
  final cameras = await availableCameras();

  // Get a specific camera from the list of available cameras.
  final firstCamera = cameras.first;

  runApp(
    MaterialApp(
      theme: ThemeData.dark(),
      home: TakePictureScreen(
        // Pass the appropriate camera to the TakePictureScreen widget.
        camera: firstCamera,
      ),
    ),
  );
}

// A screen that allows users to take a picture using a given camera.
class TakePictureScreen extends StatefulWidget {
  final CameraDescription camera;

  const TakePictureScreen({
    Key key,
    @required this.camera,
  }) : super(key: key);

  @override
  TakePictureScreenState createState() => TakePictureScreenState();
}

class TakePictureScreenState extends State<TakePictureScreen> {
  CameraController _controller;
  Future<void> _initializeControllerFuture;

  @override
  void initState() {
    super.initState();
    // To display the current output from the Camera,
    // create a CameraController.
    _controller = CameraController(
      // Get a specific camera from the list of available cameras.
      widget.camera,
      // Define the resolution to use.
      ResolutionPreset.medium,
    );

    // Next, initialize the controller. This returns a Future.
    _initializeControllerFuture = _controller.initialize();
  }

  @override
  void dispose() {
    // Dispose of the controller when the widget is disposed.
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Take a picture')),
      // Wait until the controller is initialized before displaying the
      // camera preview. Use a FutureBuilder to display a loading spinner
      // until the controller has finished initializing.
      body: FutureBuilder<void>(
        future: _initializeControllerFuture,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            // If the Future is complete, display the preview.
            return CameraPreview(_controller);
          } else {
            // Otherwise, display a loading indicator.
            return Center(child: CircularProgressIndicator());
          }
        },
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.camera_alt),
        // Provide an onPressed callback.
        onPressed: () async {
          // Take the Picture in a try / catch block. If anything goes wrong,
          // catch the error.
          try {
            // Ensure that the camera is initialized.
            await _initializeControllerFuture;

            // Construct the path where the image should be saved using the
            // pattern package.
            final path = join(
              // Store the picture in the temp directory.
              // Find the temp directory using the `path_provider` plugin.
              (await getTemporaryDirectory()).path,
              '${DateTime.now()}.png',
            );

            // Attempt to take a picture and log where it's been saved.
            await _controller.takePicture(path);

            // If the picture was taken, display it on a new screen.
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => DisplayPictureScreen(imagePath: path),
              ),
            );
          } catch (e) {
            // If an error occurs, log the error to the console.
            print(e);
          }
        },
      ),
    );
  }
}

// A widget that displays the picture taken by the user.
class DisplayPictureScreen extends StatefulWidget {
  final String imagePath;

  const DisplayPictureScreen({Key key, this.imagePath}) : super(key: key);

  @override
  _DisplayPictureScreenState createState() => _DisplayPictureScreenState();
}

class _DisplayPictureScreenState extends State<DisplayPictureScreen> {
  String result = '';
  final TextEditingController controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text('Display the Picture')),
        // The image is stored as a file on the device. Use the `Image.file`
        // constructor with the given path to display the image.
        body: ListView(
          children: <Widget>[
            Image.file(File(widget.imagePath)),
            TextFormField(
              keyboardType: TextInputType.number,
              validator: inputValidator,
              controller: controller,
              style: TextStyle(height: 2, fontSize: 20),
              decoration: new InputDecoration(
                  labelText: 'cantidad de piezas',
                  labelStyle: TextStyle(
                      color: Color(0xFF128C7E),
                      fontWeight: FontWeight.bold,
                      fontSize: 18,
                      height: 1)),
            ),
            RaisedButton(
              child: Text('calibrar'),
              onPressed: () => _calibrar(),
            ),
            RaisedButton(
              child: Text('calcular'),
              onPressed: () => _calcular(),
            ),
            Text('Resultado $result')
          ],
        ));
  }

  String inputValidator(value) {
    if (value.isEmpty) {
      return 'Este campo es requerido';
    }
    return null;
  }

  void _calibrar() {
    var file = File(widget.imagePath);
    var bytes = file.readAsBytesSync();
    var photo = img.decodeImage(bytes);
    int lightQuantity = 0;
    int darkQuantity = 0;

    for (int x = 0; x < photo.width; x++) {
      for (int y = 0; y < photo.height; y++) {
        int pixel32 = photo.getPixelSafe(x, y);
        var luma = getLuma(pixel32);
        if (luma > 128) {
          lightQuantity++;
        } else {
          darkQuantity++;
        }
      }
    }
    int total = lightQuantity + darkQuantity;
    SharedPreferences.getInstance().then((prefs) {
      prefs.setInt('lightQuantity', lightQuantity);
      prefs.setInt('darkQuantity', darkQuantity);
      prefs.setInt('quantity', int.parse(controller.text));
      prefs.setInt('total', total);
    });

    // fijar valor
    setState(() {
      result =
          'lightQuantity: $lightQuantity darkQuantity: $darkQuantity total: $total %claras: ${lightQuantity * 100 / total} %oscuras: ${darkQuantity * 100 / total}';
    });
  }

  void _calcular() {
    var file = File(widget.imagePath);
    var bytes = file.readAsBytesSync();
    var photo = img.decodeImage(bytes);
    int lightQuantity = 0;
    int darkQuantity = 0;

    for (int x = 0; x < photo.width; x++) {
      for (int y = 0; y < photo.height; y++) {
        int pixel32 = photo.getPixelSafe(x, y);
        var luma = getLuma(pixel32);
        if (luma > 128) {
          lightQuantity++;
        } else {
          darkQuantity++;
        }
      }
    }
    int total = lightQuantity + darkQuantity;
    SharedPreferences.getInstance().then((prefs) {
      prefs.getInt('lightQuantity');
      int darkQuantityCalibration = prefs.getInt('darkQuantity');
      int quantityCalibration = prefs.getInt('quantity');
      double newQuantity =
          (darkQuantity * quantityCalibration) / darkQuantityCalibration;
      setState(() {
        result =
            'cantidad aprox de fichas: ${newQuantity} lightQuantity: $lightQuantity darkQuantity: $darkQuantity total: $total %claras: ${lightQuantity * 100 / total} %oscuras: ${darkQuantity * 100 / total}';
      });
    });
  }

  // image lib uses uses KML color format, convert #AABBGGRR to regular #AARRGGBB
  int abgrToArgb(int argbColor) {
    int r = (argbColor >> 16) & 0xFF;
    int b = argbColor & 0xFF;
    return (argbColor & 0xFF00FF00) | (b << 16) | r;
  }

  double getLuma(int pixel32) {
    int hex = abgrToArgb(pixel32);
    var r = (hex >> 16) & 0xff; // extract red
    var g = (hex >> 8) & 0xff; // extract green
    var b = (hex >> 0) & 0xff; // extract blue
    var luma = 0.2126 * r + 0.7152 * g + 0.0722 * b;
    return luma;
  }
}
